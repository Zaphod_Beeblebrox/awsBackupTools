A simple AWS S3 backup scheme to backup all files and your database to an Amazon Cloud S3 Bucket.

Scripts are fairly self explanatory and shouldn't need much modification.

You will need an AWS Cloud Account, AWS CLI tools installed
