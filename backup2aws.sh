#!/bin/bash

# backup everything to AWS via CLI
# Author Andrew Carlson <andrew.carlson@overlayconsulting.com>
# License - have fun! there isn't one, call it open source, call it whatever

### Usage ###
# Configure the 3 variables of KEEP, PATHTOFILES, and BUCKET.
# Ensure that you have AWS CLI installed and you can actually talk to S3 storage
# setup backup as a cron job (example)

### Variables that control backups - feel free to edit ###

############################################
##  How many backup files should we keep? ##
##  0 means unlimited                     ##
############################################
KEEP=3 

############################################
##  Where are the files to backup?        ##
##  Absolute Paths only, like /var/www    ##
############################################
PATHTOFILES='/var/www/html'

############################################
##  A backup file is saved locally first  ##
##  where should that be?                 ##
############################################
LOCAL_STORAGE=$HOME

############################################
##  What is the name of the AWS Bucket?   ##
############################################
BUCKET="my-shiny-bucket"



### DO NOT TOUCH BELOW THIS LINE IF YOU WANT THIS TO KEEP WORKING###

## test for aws cli existence and we can see/access the bucket ##
which aws > /dev/null
if [[ $? != 0 ]]; then
  echo "Error, could not find AWS CLI"
  exit 1
fi

# test bucket is accessible, LS may not be enough of a test though
aws s3 ls s3://$BUCKET > /dev/null
if [[ $? != 0 ]]; then
  echo "Error, could confirm the bucket is accessible"
  exit 1
fi

TGT=$LOCAL_STORAGE"/" #tmp folder for backing up files before pushing to aws
DATE=`date +%Y-%m-%dT%H_%M_%S` #labeling each file with the date/time of the backup

#backup files locally, (create folder if it doesn't exist first
if [ ! -d $TGT ]; then
  mkdir -p $TGT
fi

FILE=$TGT$DATE-FILES".tar.gz"
tar zcvf $FILE $PATHTOFILES

#push to AWS S3
aws s3 cp $FILE s3://$BUCKET

# remove tmp backup file
rm $FILE

#find and remove files beyond the keep date, only if KEEP is not zero
if [[ $KEEP -ne 0 ]]; then
	PURGEDATE=`date +%Y-%m-%d -d "-$KEEP days"`
	aws s3 rm s3://$BUCKET --recursive --exclude "*" --include "$PURGEDATE*"
fi
